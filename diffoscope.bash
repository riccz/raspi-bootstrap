#!/bin/bash

set -euo pipefail

ORIG=${1}
EDIT=${2}
OUTFILE="${3:-${PWD}/raspi_diffoscope.out}"

function cleanup() {
    if mount | grep "${edit_dev}p1"; then umount "${edit_dev}p1"; fi
    if mount | grep "${edit_dev}p2"; then umount "${edit_dev}p2"; fi
    if mount | grep "${orig_dev}p1"; then umount "${orig_dev}p1"; fi
    if mount | grep "${orig_dev}p2"; then umount "${orig_dev}p2"; fi

    rmdir /mnt/raspi-bootstrap_edit || true
    rmdir /mnt/raspi-bootstrap_orig || true

    if [ -n "${edit_dev:-}" ]; then losetup -d "${edit_dev}"; fi
    if [ -n "${orig_dev:-}" ]; then losetup -d "${orig_dev}"; fi
}
trap cleanup EXIT

orig_dev=$(losetup --show -r -f "${ORIG}")
partprobe "${orig_dev}"
edit_dev=$(losetup --show -r -f "${EDIT}")
partprobe "${edit_dev}"

mkdir -p /mnt/raspi-bootstrap_orig
mkdir -p /mnt/raspi-bootstrap_edit

mount -o ro "${orig_dev}p2" /mnt/raspi-bootstrap_orig
mount -o ro "${orig_dev}p1" /mnt/raspi-bootstrap_orig/boot
mount -o ro "${edit_dev}p2" /mnt/raspi-bootstrap_edit
mount -o ro "${edit_dev}p1" /mnt/raspi-bootstrap_edit/boot

pushd /mnt/raspi-bootstrap_edit/ >/dev/null
diffoscope  --exclude './dev/*' \
            --exclude './proc/*' \
            --exclude './sys/*' \
            --text "${OUTFILE}" \
            /mnt/raspi-bootstrap_orig/ ./ || true
popd >/dev/null
